# VotingApp Service for MSYS Workshop

## Run as docker container

### Prerequisites
docker, docker-compose, docker-machine

VotingApp Service also needs a running Redis docker
container, running under following name:

- redis

(e.g. docker run --name redis -p 6379:6379 -d redis)

### Build and run

```
$ mvn clean install
$ docker-compose build
$ docker-compose up -d
### Optional: 
### $ docker-compose logs (to see start up logs)
```

### Verify, it is running:
 - http://DOCKER_MACHINE_IP:8080/voting-app/health

### Stop docker container:

```
$ docker-compose stop
$ docker-compose rm
```

### Checkout Hystrix Dashboard
 - http://DOCKER_MACHINE_IP:8080/voting-app/hystrix.monitor
 ### Enter in the monitor url: - http://DOCKER_MACHINE_IP:8080/voting-app/hystrix.stream
 
### Checkout Swagger UI
 - http://DOCKER_MACHINE_IP:8080/voting-app/swagger/index.html
 
### Checkout REST Endpoints
	- curl -X POST -H "Content-Type: application/json" -d "{\"color\" : \"magenta\"}" http://DOCKER_MACHINE_IP:8080/voting-app/votes
	- curl -X GET -H "Accept: application/json" http://DOCKER_MACHINE_IP:8080/voting-app/votes
	
### Stress it a little bit
    - ab -n 30 -c 30 -t 30 http://192.168.1.2:32635/voting-app/votes
    - ab -p test.data -c 10 -n 10 -t 30 -T application/json http://192.168.1.2:32635/voting-app/votes
and store into test.data {"color":"green"}


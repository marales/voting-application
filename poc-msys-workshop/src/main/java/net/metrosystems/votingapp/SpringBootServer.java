package net.metrosystems.votingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
/**
 * *Enable auto-configuration of the Spring Application Context, attempting to
 * guess and configure beans that you are likely to need.
 */
@EnableAutoConfiguration
@ComponentScan(basePackages = {"net.metrosystems.votingapp"})
@EnableCircuitBreaker
@EnableHystrixDashboard
@EnableSwagger2
public class SpringBootServer {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootServer.class, args);
    }

}

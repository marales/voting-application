package net.metrosystems.votingapp.configuration;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.netflix.hystrix.strategy.HystrixPlugins;
import com.netflix.hystrix.strategy.concurrency.HystrixConcurrencyStrategy;

@Configuration
@PropertySources(value = { @PropertySource("classpath:application.properties") })
public class ApplicationConfiguration {

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }

    @Bean
    public Filter logbackFilter() {
        LogbackFilter logbackFilter = new LogbackFilter();
        return logbackFilter;
    }

    @Bean
    public HystrixConcurrencyStrategy concurrencyStrategy() {
        return new LoggingConcurrencyStrategy();
    }

    @PostConstruct
    public void configLogbackMetrics() {
        HystrixPlugins.getInstance().registerConcurrencyStrategy(concurrencyStrategy());
    }
    
	@Bean
	@ConditionalOnMissingBean
	public AuthenticationProvider authenticationProvider() {
		return new DefaultAuthenticationProvider();
	}

	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
package net.metrosystems.votingapp.configuration;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class DefaultAuthenticationProvider implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return authentication;
    }

    /**
     * Returns true if this AuthenticationProvider supports the indicated
     * Authentication object.
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(Authentication.class);
    }

}

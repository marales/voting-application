package net.metrosystems.votingapp.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisPoolWrapper {
	final static Logger logger = LoggerFactory.getLogger(JedisPoolWrapper.class);
	
	private Environment env;
	
	private volatile JedisPool jedisPool = null;
	
	public JedisPoolWrapper(Environment env) {
		this.env = env;		
	}
	
	private synchronized JedisPool initializeJedisPool(){
		try{
			JedisPoolConfig poolConfig = new JedisPoolConfig();
		    poolConfig.setMaxTotal(50);
		    poolConfig.setBlockWhenExhausted(true);
		    jedisPool = new JedisPool(poolConfig, env.getProperty("redis.contact.point"), 
		    		env.getProperty("redis.contact.port")==null?6379:new Integer(env.getProperty("redis.contact.port")));
		}catch(Throwable t){
			logger.error(t.getMessage(), t);
			jedisPool = null;
			throw t;
		}
		return jedisPool;
	}
	
	public synchronized JedisPool getJedisPool(){
		if (jedisPool == null){
			initializeJedisPool();
		}
		return jedisPool;
	}
}

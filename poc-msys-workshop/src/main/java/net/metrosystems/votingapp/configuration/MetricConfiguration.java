package net.metrosystems.votingapp.configuration;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import com.codahale.metrics.JmxReporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;

import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;
import com.codahale.metrics.logback.InstrumentedAppender;
import com.ryantenney.metrics.spring.config.annotation.EnableMetrics;

@Configuration
@EnableMetrics
public class MetricConfiguration {

    @Value("${metrics.slf4j.enabled}")
    private boolean slf4jEnabled;

    @Autowired
    private MetricRegistry metricRegistry;

    @Bean
    public Slf4jReporter slf4jReporter() {
        final MetricFilter filter;
        if (slf4jEnabled) {
            filter = MetricFilter.ALL;
        } else {
            filter = (name, metric) -> false;
        }
        final Slf4jReporter reporter = Slf4jReporter.forRegistry(metricRegistry)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .filter(filter)
                .build();
        reporter.start(30, TimeUnit.SECONDS);
        return reporter;
    }

    @Bean
    public JmxReporter jmxReporter() {
        final JmxReporter reporter=JmxReporter.forRegistry(metricRegistry).build();
        reporter.start();
        return reporter ;
    }

    @PostConstruct
    public void configLogbackMetrics()
    {
        final LoggerContext factory = (LoggerContext) org.slf4j.LoggerFactory.getILoggerFactory();
        final Logger root = factory.getLogger(Logger.ROOT_LOGGER_NAME);

        final InstrumentedAppender metrics = new InstrumentedAppender(metricRegistry);
        metrics.setContext(root.getLoggerContext());
        metrics.start();

        root.addAppender(metrics);
    }
}


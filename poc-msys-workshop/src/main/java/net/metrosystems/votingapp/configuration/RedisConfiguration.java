package net.metrosystems.votingapp.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class RedisConfiguration {

    final static Logger logger = LoggerFactory.getLogger(RedisConfiguration.class);

    @Autowired
    private Environment env;

    @Bean
    public JedisPoolWrapper jedisPool(){
    	return new JedisPoolWrapper(env);
    }
    
}

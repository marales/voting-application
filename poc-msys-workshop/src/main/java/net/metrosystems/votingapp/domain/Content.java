package net.metrosystems.votingapp.domain;

import java.util.List;
import java.util.Map;

public class Content {

    private Map<String, String> votes;    
    private String ipAddress;
    private List<Details> details;

    public Content(){
        
    }
    
    public Map<String, String> getVotes() {
        return votes;
    }
    public void setVotes(Map<String, String> votes) {
        this.votes = votes;
    }
    public String getIpAddress() {
        return ipAddress;
    }
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
    public List<Details> getDetails() {
        return details;
    }
    public void setDetails(List<Details> details) {
        this.details = details;
    }
   
}
package net.metrosystems.votingapp.domain;

import java.util.Date;

public class Details {
    
    private String ipAddress;
    private Date voteDate;
    private Vote vote;
    
    public Details(){
        
    }
    
    public Details(String ipAddress, Date voteDate, Vote vote) {
        super();
        this.ipAddress = ipAddress;
        this.voteDate = voteDate;
        this.vote = vote;
    }
    
    public String getIpAddress() {
        return ipAddress;
    }
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
    public Date getVoteDate() {
        return voteDate;
    }
    public void setVoteDate(Date voteDate) {
        this.voteDate = voteDate;
    }
    public Vote getVote() {
        return vote;
    }
    public void setVote(Vote vote) {
        this.vote = vote;
    }   

}
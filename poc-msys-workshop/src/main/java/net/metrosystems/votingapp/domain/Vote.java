package net.metrosystems.votingapp.domain;

public class Vote {

    String color;

    public Vote() {
    }
    
    public Vote(String color) {
        super();
        this.color = color;
    }
    
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Vote [color=" + color + "]";
    }

}

package net.metrosystems.votingapp.exceptions;

import java.util.Optional;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.MDC;

import com.eaio.uuid.UUID;
import com.netflix.hystrix.exception.HystrixRuntimeException;

import net.metrosystems.votingapp.configuration.LogbackFilter;

public class ErrorMessages {

    public static final String MDC_REQUEST_ID_KEY = LogbackFilter.REQUEST_ID;
	
	public static class TrackableInformation {
	    
	    String message;
	    String id;
	    
	    public TrackableInformation(String message, String id) {
	        this.message = message;
	        this.id = id;
	    }

        public String getMessage() {
            return message;
        }

        public String getId() {
            return id;
        }
	    
	}
	
    public static TrackableInformation getTrackableInformation(HystrixRuntimeException exception) {
        
        Throwable fallbackException = exception.getFallbackException();
        String errorMsg = fallbackException != null? fallbackException.getMessage() : exception.getMessage();
        UUID errorID = fallbackException != null && (fallbackException instanceof TrackableServiceException)? ((TrackableServiceException)fallbackException).getId() : null;
        
        if(fallbackException != null && errorID == null) {
            Optional<Throwable> firstTrackable = ExceptionUtils.getThrowableList(fallbackException).stream().filter(t -> t instanceof TrackableServiceException).findFirst();
            if(firstTrackable.isPresent()) {
                errorMsg = firstTrackable.get().getMessage();
                errorID = ((TrackableServiceException)(firstTrackable.get())).getId();
            }
        }
        
        return new TrackableInformation(errorMsg, errorID != null? errorID.toString() : null);
    }	
    
    public static <T> T throwFallbackException(String message) throws RuntimeException {
        String requestID = MDC.get(MDC_REQUEST_ID_KEY);
        ServiceUnavailableException serviceUnavailableException =
                requestID != null ? new ServiceUnavailableException(message, new UUID(requestID)) : new ServiceUnavailableException(message);
        throw new RuntimeException(serviceUnavailableException);
    }
}

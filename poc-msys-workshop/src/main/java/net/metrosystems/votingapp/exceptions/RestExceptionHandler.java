package net.metrosystems.votingapp.exceptions;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.netflix.hystrix.exception.HystrixRuntimeException;

import net.metrosystems.votingapp.exceptions.ErrorMessages.TrackableInformation;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(HystrixRuntimeException.class)
    @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
    @ResponseBody
    //TODO: if possible, add header Retry-After: int in sec. ... as a hint for the client
    // value should be at least circuitBreaker.sleepWindowInMilliseconds (configurable or in the ServiceUnavailableException)
    // take care of auto-DDoS - don't set value to fix time, to avoid simultaneous calls of many clients!
    public CustomError handleHystrixRuntimeException(HystrixRuntimeException exception) {
        TrackableInformation trackableInformation = ErrorMessages.getTrackableInformation(exception);
        return new CustomError(trackableInformation.getMessage(), trackableInformation.getId(), new Date(System.currentTimeMillis()));
    }
    
}

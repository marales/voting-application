package net.metrosystems.votingapp.exceptions;

import com.eaio.uuid.UUID;

public class ServiceUnavailableException extends TrackableServiceException {

	private static final long serialVersionUID = 5546076857348888621L;

	public ServiceUnavailableException() {
		super();
	}

	public ServiceUnavailableException(UUID id) {
		super(id);
	}

	public ServiceUnavailableException(String message) {
		super(message);
	}

	public ServiceUnavailableException(String message, UUID id) {
		super(message, id);
	}

	@Override
	protected String getDefaultErrorMessage() {
		return "Service unavailable exception";
	}


}

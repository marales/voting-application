package net.metrosystems.votingapp.exceptions;

import com.eaio.uuid.UUID;

public abstract class TrackableServiceException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 2105072836767686405L;
    
    private String message;
    private UUID id;

    @Deprecated
    public TrackableServiceException() {
        this.message = getDefaultErrorMessage();
        this.id = new UUID();
    }

    public TrackableServiceException(UUID id) {
        this.message = getDefaultErrorMessage();
        this.id = id;
    }

    @Deprecated
    public TrackableServiceException(String message) {
        this.message = message;
        this.id = new UUID();
    }

    public TrackableServiceException(String message, UUID id) {
        this.message = message;
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
    
    protected abstract String getDefaultErrorMessage();

}

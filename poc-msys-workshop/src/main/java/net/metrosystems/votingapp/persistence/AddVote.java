package net.metrosystems.votingapp.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import net.metrosystems.votingapp.configuration.JedisPoolWrapper;
import net.metrosystems.votingapp.domain.Vote;
import redis.clients.jedis.Jedis;

@Component
public class AddVote {
    
    Logger logger = LoggerFactory.getLogger(AddVote.class);
    
    @Autowired
    JedisPoolWrapper jedisPoolWrapper;
    
    @HystrixCommand(commandKey = "AddVote", fallbackMethod = "addVoteFallback")
    public boolean execute(Vote vote) {
        logger.info("AddVote.execute adding vote={}", vote);
        Jedis jedis = null;
        try{        
        	jedis = jedisPoolWrapper.getJedisPool().getResource();
        	jedis.incr(vote.getColor());
        	
        }catch (Throwable t){
        	logger.error(t.getMessage(), t);
        }finally{
        	if (jedis != null){
        		jedis.close();
        	}
        }
        return true;
    }
    
    public boolean addVoteFallback(Vote vote, Throwable t) {
        if (t != null) {
            logger.warn("AddVote.addVoteFallback hystrix command dashboard error", t.getCause());
        } else {
            logger.warn("AddVote.addVoteFallback hystrix command error is unknown");
        }
      
        return false;
    }

}

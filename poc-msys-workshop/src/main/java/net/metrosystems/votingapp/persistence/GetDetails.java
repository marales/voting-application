package net.metrosystems.votingapp.persistence;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import net.metrosystems.votingapp.configuration.JedisPoolWrapper;
import redis.clients.jedis.Jedis;

@Component
public class GetDetails {

    private Logger logger = LoggerFactory.getLogger(GetDetails.class);

    @Autowired
    JedisPoolWrapper jedisPoolWrapper;
    
    @HystrixCommand(commandKey = "GetVotes", fallbackMethod = "getDetailsFallback")
    public List<String> execute() {

        Jedis jedis = null;
        try{        	
        	jedis = jedisPoolWrapper.getJedisPool().getResource();
        	List<String> detailsList = jedis.lrange("details", 0, 9);
            return detailsList;
        }catch (Throwable t){
        	logger.error(t.getMessage(), t);
        	return new ArrayList<String>();
        }finally{
        	if (jedis != null){
        		jedis.close();
        	}
        }
    }

    public List<String> getDetailsFallback(Throwable t) {
        if (t != null) {
            logger.error("GetVotes.getVotesFallback hystrix command dashboard error", t.getCause());
        } else {
            logger.error("GetVotes.getVotesFallback hystrix command error is unknown");
        }
        return new ArrayList<>();
    }
}
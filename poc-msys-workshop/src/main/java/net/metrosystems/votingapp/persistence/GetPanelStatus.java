package net.metrosystems.votingapp.persistence;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import net.metrosystems.votingapp.configuration.JedisPoolWrapper;
import redis.clients.jedis.Jedis;

@Component
public class GetPanelStatus {

    Logger logger = LoggerFactory.getLogger(GetPanelStatus.class);
    
    @Autowired
    JedisPoolWrapper jedisPoolWrapper;
    
    private enum panels {red, blue, green};

    @HystrixCommand(commandKey = "GetPanelStatus", fallbackMethod = "getPanelStatusFallback")
    public Map<String, String> getPanelStatus() {
        logger.info("GetPanelStatus.getPanelStatus try reading votes map");
        // Get the stored data and print it
        Map<String, String> votes = new HashMap<>();
        Jedis jedis = null;
        try{        	      	
        	jedis = jedisPoolWrapper.getJedisPool().getResource();
        	Set<String> list = jedis.keys("*[^details]*");
        	final Jedis jedisF = jedis; 
        	list.iterator().forEachRemaining(key -> votes.put(key, jedisF.get(key)));
        }catch (Throwable t){
        	logger.error(t.getMessage(), t);
        }finally{
        	if (jedis != null){
        		jedis.close();
        	}
        }
        
		if (!areKeysValid(votes)){
			logger.error("Wrong data in database. Check the content");			
			return getDefaultStatus();
		}
		
		Map<String, String> result = computeStatus(votes);
		
		return result;
    }

    private boolean areKeysValid(Map<String, String> votes){
    	return isValidValue(votes.get(panels.blue.name())) &&
				isValidValue(votes.get(panels.red.name())) &&
				isValidValue(votes.get(panels.green.name()));
    }
    
    private Map<String, String> computeStatus(Map<String, String> votes){
    	Integer blue = new Integer (votes.get("blue"));
		Integer red = new Integer (votes.get("red"));
		Integer green = new Integer (votes.get("green"));
		
		double thresholdPercentage = 0.7d;
		int thresholdNo = 20;
		
		int total = blue + green + red;
		
		Boolean greenStatus = (double)green / (double)total < thresholdPercentage || green < thresholdNo;
		Boolean redStatus = (double)red / (double)total < thresholdPercentage || red < thresholdNo;
		Boolean blueStatus = (double)blue / (double)total < thresholdPercentage || blue < thresholdNo;
		
		votes.put("green", greenStatus.toString());
		votes.put("red", redStatus.toString());
		votes.put("blue", blueStatus.toString());
		
		return votes;
    }
    
    private Map<String, String> getDefaultStatus(){
    	Map<String, String> defaultStatus = new HashMap<String, String>();
    	defaultStatus.put("green", Boolean.TRUE.toString());
    	defaultStatus.put("red", Boolean.TRUE.toString());
    	defaultStatus.put("blue", Boolean.TRUE.toString());
    	return defaultStatus;
    }
    
    public Map<String, String> getPanelStatusFallback(Throwable t) {
        if (t != null) {
            logger.warn("GetPanelStatus.getPanelStatusFallback hystrix command dashboard error", t.getCause());
        } else {
            logger.warn("GetPanelStatus.getPanelStatusFallback hystrix command error is unknown");
        }
        
        return getDefaultStatus();
    }

    private boolean isValidValue(String value){
		try{
			return value != null && !value.isEmpty() && new Integer(value) >=0;
		}catch(NumberFormatException ne){
			return false;
		}
	}
}

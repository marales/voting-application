package net.metrosystems.votingapp.persistence;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import net.metrosystems.votingapp.configuration.JedisPoolWrapper;
import redis.clients.jedis.Jedis;

@Component
public class GetVotes {

    Logger logger = LoggerFactory.getLogger(GetVotes.class);
    
    @Autowired
    JedisPoolWrapper jedisPoolWrapper;
    

    @HystrixCommand(commandKey = "GetVotes", fallbackMethod = "getVotesFallback")
    public Map<String, String> getVotes() {
        logger.info("GetVotes.getVotes try reading votes map");
        // Get the stored data and print it
        Map<String, String> votes = new HashMap<>();
        Jedis jedis = null;
        try{        	
        	
        	jedis = jedisPoolWrapper.getJedisPool().getResource();
        	Set<String> list = jedis.keys("*[^details]*");
        	final Jedis jedisF = jedis; 
        	list.iterator().forEachRemaining(key -> votes.put(key, jedisF.get(key)));
        }catch (Throwable t){
        	logger.error(t.getMessage(), t);
        }finally{
        	if (jedis != null){
        		jedis.close();
        	}
        }
        return votes;
    }
    
    public Map<String, String> getVotesFallback(Throwable t) {
        if (t != null) {
            logger.warn("GetVotes.getVotesFallback hystrix command dashboard error", t.getCause());
        } else {
            logger.warn("GetVotes.getVotesFallback hystrix command error is unknown");
        }
        
        return Collections.EMPTY_MAP;
    }

}

package net.metrosystems.votingapp.persistence;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import net.metrosystems.votingapp.configuration.JedisPoolWrapper;
import redis.clients.jedis.Jedis;

@Component
public class PowerControl {

    Logger logger = LoggerFactory.getLogger(PowerControl.class);
    
    @Autowired
    JedisPoolWrapper jedisPoolWrapper;
    
    @Autowired
    RestTemplate restTemplate;
    
    @Autowired
    private Environment env;
    
    private enum panels {red, blue, green};

    @HystrixCommand(commandKey = "GetPanelStatus", fallbackMethod = "getPanelStatusFallback")
    public Map<String, String> setPanelStatus() {
        logger.info("PowerControl.getPanelStatus try reading votes map");
        // Get the stored data and print it
        Map<String, String> votes = new HashMap<>();
        Jedis jedis = null;
        try{        	      	
        	jedis = jedisPoolWrapper.getJedisPool().getResource();
        	Set<String> list = jedis.keys("*[^details]*");
        	final Jedis jedisF = jedis; 
        	list.iterator().forEachRemaining(key -> votes.put(key, jedisF.get(key)));
        }catch (Throwable t){
        	logger.error(t.getMessage(), t);
        }finally{
        	if (jedis != null){
        		jedis.close();
        	}
        }
        
		if (!areKeysValid(votes)){
			logger.error("Wrong data in database. Check the content");			
			return getDefaultStatus();
		}
		
		Map<String, String> result = computeRequiredStatus(votes);
		setPowerStatus(result);
		return result;
    }
    
    @HystrixCommand(commandKey = "PowerControl")
    private Map<String, String> setPowerStatus(Map<String, String> requiredStatus) {
        logger.info("PowerControl.setPowerStatus try reading votes map");
        int currentStatus = readPowerSwitchStatus();
        int green = currentStatus & 1;
        int blue = currentStatus & 2;
        int red = currentStatus & 4;
        
        if ((green == 0 && requiredStatus.get("green").equals("true")) ||
        		(green == 1 && requiredStatus.get("green").equals("false"))){
        	setPowerSwitchStatus(1);
        }
        if ((blue == 0 && requiredStatus.get("blue").equals("true")) ||
        		(blue == 2 && requiredStatus.get("blue").equals("false"))){
        	setPowerSwitchStatus(2);
        }
        if ((red == 0 && requiredStatus.get("red").equals("true")) ||
        		(red == 4 && requiredStatus.get("red").equals("false"))){
        	setPowerSwitchStatus(3);
        }
        
        return requiredStatus;
    }

    private int readPowerSwitchStatus(){
    	String url ="http://"+env.getProperty("power.control.ip")+"/httpapi.json?CMD=UART_WRITE&UWHEXVAL=0";
    	return executeRestCall(url);
    }

    private int setPowerSwitchStatus(int position){
    	String url ="http://"+env.getProperty("power.control.ip")+"/httpapi.json?CMD=UART_WRITE&UWHEXVAL=" + position;
    	return executeRestCall(url);
    }
    
    private int executeRestCall(String url){
    	HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getAuthzHeaderValue("admin", "admin"));
        headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
        HttpEntity requestEntity = new HttpEntity<>(headers);
        UriComponentsBuilder urlBuilder =
                UriComponentsBuilder.fromHttpUrl(url);
        ResponseEntity<String> responseEntity = restTemplate.exchange(urlBuilder.build().encode().toUri(),
                HttpMethod.GET, requestEntity, String.class);
        String res = responseEntity.getBody();
        return new Integer(res);
    }
    
    private static String getAuthzHeaderValue(String username, String password) {
        String plainCreds = username + ":" + password;
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encode(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);
        return "Basic " + base64Creds;
    }
    
    private boolean areKeysValid(Map<String, String> votes){
    	return isValidValue(votes.get(panels.blue.name())) &&
				isValidValue(votes.get(panels.red.name())) &&
				isValidValue(votes.get(panels.green.name()));
    }
    
    private Map<String, String> computeRequiredStatus(Map<String, String> votes){
    	Integer blue = new Integer (votes.get("blue"));
		Integer red = new Integer (votes.get("red"));
		Integer green = new Integer (votes.get("green"));
		
		double thresholdPercentage = 0.7d;
		int thresholdNo = 20;
		
		int total = blue + green + red;
		
		Boolean greenStatus = (double)green / (double)total < thresholdPercentage || green < thresholdNo;
		Boolean redStatus = (double)red / (double)total < thresholdPercentage || red < thresholdNo;
		Boolean blueStatus = (double)blue / (double)total < thresholdPercentage || blue < thresholdNo;
		
		votes.put("green", greenStatus.toString());
		votes.put("blue", blueStatus.toString());
		votes.put("red", redStatus.toString());
		
		return votes;
    }
    
    private Map<String, String> getDefaultStatus(){
    	Map<String, String> defaultStatus = new HashMap<String, String>();
    	defaultStatus.put("green", Boolean.TRUE.toString());
    	defaultStatus.put("red", Boolean.TRUE.toString());
    	defaultStatus.put("blue", Boolean.TRUE.toString());
    	return defaultStatus;
    }
    
    private Map<String, String> getPanelStatusFallback(Throwable t) {
        if (t != null) {
            logger.warn("GetPanelStatus.getPanelStatusFallback hystrix command dashboard error", t.getCause());
        } else {
            logger.warn("GetPanelStatus.getPanelStatusFallback hystrix command error is unknown");
        }
        
        return getDefaultStatus();
    }

    private boolean isValidValue(String value){
		try{
			return value != null && !value.isEmpty() && new Integer(value) >=0;
		}catch(NumberFormatException ne){
			return false;
		}
	}
}

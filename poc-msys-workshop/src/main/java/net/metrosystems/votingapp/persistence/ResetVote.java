package net.metrosystems.votingapp.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import net.metrosystems.votingapp.configuration.JedisPoolWrapper;
import net.metrosystems.votingapp.domain.Vote;
import redis.clients.jedis.Jedis;

@Component
public class ResetVote {
    
    Logger logger = LoggerFactory.getLogger(ResetVote.class);
    
    @Autowired
    JedisPoolWrapper jedisPoolWrapper;
    
    @HystrixCommand(commandKey = "ResetVote", fallbackMethod = "resetVoteFallback")
    public boolean execute(Vote vote) {
        logger.info("ResetVote.execute");
        Jedis jedis = null;
        try{        	
        	jedis = jedisPoolWrapper.getJedisPool().getResource();
        	jedis.set(vote.getColor(), "10");
        }catch (Throwable t){
        	logger.error(t.getMessage(), t);
        }finally{
        	if (jedis != null){
        		jedis.close();
        	}
        }
        return true;
    }
    
    public boolean resetVoteFallback(Vote vote, Throwable t) {
        if (t != null) {
            logger.warn("ResetVote.resetVoteFallback hystrix command dashboard error", t.getCause());
        } else {
            logger.warn("ResetVote.resetVoteFallback hystrix command error is unknown");
        }
      
        return false;
    }

}

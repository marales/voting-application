package net.metrosystems.votingapp.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import net.metrosystems.votingapp.configuration.JedisPoolWrapper;
import redis.clients.jedis.Jedis;

@Component
public class SaveDetails {
    private Logger logger = LoggerFactory.getLogger(SaveDetails.class);
    
    @Autowired
    JedisPoolWrapper jedisPoolWrapper;
    
    @HystrixCommand(commandKey = "SaveDetails", fallbackMethod = "saveDetailsFallback")
    public boolean execute(String details) {
        Jedis jedis = null;
        try{        	
        	jedis = jedisPoolWrapper.getJedisPool().getResource();
        	jedis.lpush("details", details);
        }catch (Throwable t){
        	logger.error(t.getMessage(), t);
        }finally{
        	if (jedis != null){
        		jedis.close();
        	}
        }
        return true;
    }
    
    public boolean saveDetailsFallback(String details, Throwable t) {
        if (t != null) {
            logger.error("SaveDetails.saveDetailsFallback hystrix command dashboard error", t.getCause());
          } else {
            logger.error("SaveDetails.saveDetailsFallback hystrix command error is unknown");
        }
        return false;
    }    
}
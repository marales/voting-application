package net.metrosystems.votingapp.rest;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.metrosystems.votingapp.domain.Content;
import net.metrosystems.votingapp.domain.Details;
import net.metrosystems.votingapp.domain.Vote;
import net.metrosystems.votingapp.service.VotingService;

@RestController
public class VotingAppRestController {

    Logger logger = LoggerFactory.getLogger(VotingAppRestController.class);

    @Autowired
    VotingService votingService;

    @ApiOperation(
            value = "Get the number of votes per color",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Number of votes per color") })
    @RequestMapping(method = RequestMethod.GET, path = "/votes", produces = "application/json")
    public Content getVotesNo() {
        logger.info("Endpoint GET /votes entered");
        Content content = new Content();
        Map<String, String> votes = votingService.getVotes();
        List<Details> details = votingService.getDetails();
        content.setVotes(votes);
        content.setDetails(details);
        content.setIpAddress(getIpAddress());

        return content;
    }

    
    private String getIpAddress() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (Throwable e) {
        	//ignore error
//            e.printStackTrace();
        }

        return "Unknown";
    }

    @ApiOperation(
            value = "Creates a new vote",
            consumes = "application/json",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The vote was successfully added") })
    @RequestMapping(method = RequestMethod.POST, path = "/votes", consumes = "application/json", produces = "application/json")
    public Map<String, String> addVote(@RequestBody Vote vote, HttpServletRequest httpServlet) {
        logger.info("Endpoint POST /votes entered");
        votingService.addVote(vote);

        //remove it because the IP is always the load balancer IP :(
//        Details details = new Details(httpServlet.getRemoteAddr(), new Date(System.currentTimeMillis()), vote);
//        votingService.saveDetails(details);
        
        Map<String, String> result = new HashMap<>();
        result.put("instanceColor", getInstanceColor());
        return result;
    }

    private String getInstanceColor() {
        String[] colors = new String[]{ "green", "red", "blue" };
        Random rand = new Random();
        return colors[rand.nextInt(2)];
    }
    
    @ApiOperation(
            value = "Reset vote",
            consumes = "application/json",
            produces = "text/plain")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The vote was successfully reset") })
    @RequestMapping(method = RequestMethod.POST, path = "/resetvote", consumes = "application/json", produces = "text/plain")
    public String resetVote(@RequestBody Vote vote) {
        logger.info("Endpoint POST /resetvote entered");
        votingService.resetVote(vote);
        return "Succesfully reset";
    }
    
    @ApiOperation(
            value = "Set panel status",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The vote was successfully reset") })
    @RequestMapping(method = RequestMethod.POST, path = "/panelstatus", produces = "application/json")
    public Map<String, String> setPanelStatus() {
        logger.info("Endpoint POST /panelstatus entered");
        return votingService.setPaneStatus();
    }
    
    @ApiOperation(
            value = "Get panel status",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get the status for each panel") })
    @RequestMapping(method = RequestMethod.GET, path = "/panelstatus", produces = "application/json")
    public Map<String, String> getPanelStatus() {
        logger.info("Endpoint GET /panelstatus entered");
        Map<String, String> result = votingService.getPaneStatus();
        return result;
    }
}

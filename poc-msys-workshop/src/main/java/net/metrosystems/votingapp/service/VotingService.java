package net.metrosystems.votingapp.service;

import java.util.List;
import java.util.Map;

import net.metrosystems.votingapp.domain.Details;
import net.metrosystems.votingapp.domain.Vote;

public interface VotingService {
    
    boolean addVote(Vote vote);

    Map<String, String> getVotes();
    
    Map<String, String> getPaneStatus();
    
    Map<String, String> setPaneStatus();
    
    boolean resetVote(Vote vote);
    
    void saveDetails(Details details);
    
    List<Details> getDetails();

}

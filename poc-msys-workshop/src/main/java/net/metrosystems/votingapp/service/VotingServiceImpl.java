package net.metrosystems.votingapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.metrosystems.votingapp.domain.Details;
import net.metrosystems.votingapp.domain.Vote;
import net.metrosystems.votingapp.persistence.AddVote;
import net.metrosystems.votingapp.persistence.GetDetails;
import net.metrosystems.votingapp.persistence.GetPanelStatus;
import net.metrosystems.votingapp.persistence.GetVotes;
import net.metrosystems.votingapp.persistence.PowerControl;
import net.metrosystems.votingapp.persistence.ResetVote;
import net.metrosystems.votingapp.persistence.SaveDetails;
import net.metrosystems.votingapp.util.DetailsUtil;

@Service
public class VotingServiceImpl implements VotingService {
    
    Logger logger = LoggerFactory.getLogger(VotingServiceImpl.class);
    
    @Autowired
    AddVote addVote;
    
    @Autowired
    GetVotes getVotes;
    
    @Autowired
    ResetVote resetVote;

    @Autowired
    GetPanelStatus panelStatus;
    
    @Autowired
    SaveDetails saveDetails;

    @Autowired
    GetDetails getDetails;
    
    @Autowired
    PowerControl powerControl;
    
    @Override
    public boolean addVote(Vote vote) {
        logger.info("VotingServiceImpl.addVote vote={}", vote);
        addVote.execute(vote);
        return true;
    }

    @Override
    public Map<String, String> getVotes() {
        return getVotes.getVotes();
    }

    @Override
    public boolean resetVote(Vote vote) {
    	resetVote.execute(vote);
    	return true;
    }

	@Override
	public Map<String, String> getPaneStatus() {
		return panelStatus.getPanelStatus();
	}

	@Override
    public void saveDetails(Details details) {
        String value = DetailsUtil.serialize(details);
        saveDetails.execute(value);
    }

    public List<Details> mapStringsToDetails(List<String> list) {
        List<Details> details = new ArrayList<>();
        list.stream().forEach(e -> details.add(DetailsUtil.deserialize(e)));

        return details;
    }

    @Override
    public List<Details> getDetails() {
        List<String> list = getDetails.execute();
        List<Details> details = mapStringsToDetails(list);

        return details;
    }

	@Override
	public Map<String, String> setPaneStatus() {
		return powerControl.setPanelStatus();
	}
}

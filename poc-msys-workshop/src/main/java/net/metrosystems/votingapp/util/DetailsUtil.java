package net.metrosystems.votingapp.util;

import java.util.Date;

import net.metrosystems.votingapp.domain.Details;
import net.metrosystems.votingapp.domain.Vote;

public class DetailsUtil {

    
    public static String serialize(Details details){
        return details.getIpAddress() + "#" + details.getVoteDate().getTime() + "#" + details.getVote().getColor();
    }
    
    public static Details deserialize(String content){
        String[] contentList = content.split("#");
        Details details = new Details();
        details.setIpAddress(contentList[0]);
        details.setVoteDate(new Date(Long.parseLong(contentList[1])));
        details.setVote(new Vote(contentList[2]));
        
        return details;
    }
    
}
var bgColors = {
    blue : "#337ab7",
    red : "#d9534f",
    green : "#5cb85c"
} ;

$(document).ready(function () {
    init();
    refresh();

    $("#blue, #green, #red").click(function () {
        var colorSelected = $(this).attr("id");
        disableButton(colorSelected);
        $.ajax({
            method: "POST",
            contentType: 'application/json',
            url: "/voting-app/votes",
            data: JSON.stringify({color: colorSelected})
        })
            .done(function (data) {
                console.log('Vote saved');
                updateBgColor(data.instanceColor);
                refresh();
            }).fail(function () {
            console.log("Vote failed");
        }).always(function () {
            enableButton(colorSelected);
        });
    });

    setInterval(refresh, 2000);

});

function init() {
    $("#circle-blue").circliful({
        animation: 0,
        foregroundBorderWidth: 5,
        backgroundBorderWidth: 15,
        percent: 0
    });

    $("#circle-red").circliful({
        animation: 0,
        foregroundColor: "#d9534f",
        foregroundBorderWidth: 5,
        backgroundBorderWidth: 15,
        percent: 0
    });

    $("#circle-green").circliful({
        animation: 0,
        foregroundColor: "#5cb85c",
        foregroundBorderWidth: 5,
        backgroundBorderWidth: 15,
        percent: 0
    });
};

function refresh() {
    $.getJSON("/voting-app/votes", function (data) {
        $.each(data, function (key, value) {
            if (key === 'ipAddress') {
                changeIP(value);
            } else if (key === 'votes') {
                updateVotes(value);
            } else if (key === 'details') {
                updateDetails(value);
            }
        });
    });
};

function updateDetails(details) {
    $('#table-body').empty();

    for (var i = 0; i < details.length; i++) {
        updateTable(details[i].ipAddress, details[i].voteDate, details[i].vote.color);
    }
}

function updateVotes(colors) {
    var totalVotes = 0;

    for (var color in colors) {
        if (colors.hasOwnProperty(color)) {
            totalVotes += parseInt(colors[color]);
        }
    }

    for (var color in colors) {
        if (colors.hasOwnProperty(color)) {
            changePercent(color, calPercent(colors[color], totalVotes));
            changeLabel(color, colors[color]);
        }
    }
};

function changePercent(color, percent) {
    var id = '#circle-' + color;
    var forColor;

    if (color === 'blue') {
        forColor = "#3498DB";
    } else if (color === 'red') {
        forColor = "#d9534f";
    } else if (color == 'green') {
        forColor = "#5cb85c";
    }

    $(id).empty().removeData().attr('data-percent', '[' + percent + ']').circliful({
        animation: 0,
        foregroundColor: forColor,
        foregroundBorderWidth: 5,
        backgroundBorderWidth: 15,
        percent: percent
    });
};

function changeIP(ip) {
    $('#ip-address').text(ip);
};

function calPercent(vote, total) {
    return parseFloat((vote / total) * 100).toFixed(0);
};

function changeLabel(color, votes) {
    $('#' + color + '-label').text(votes + ' votes');
};

function disableButton(color) {
    $('#' + color).addClass("disabled");
};

function enableButton(color) {
    $('#' + color).removeClass("disabled");
};

function updateTable(ip, date, color) {
    $('#table-body').append('<tr><td>' + ip + '</td><td>' + formatDate(new Date(date)) + '</td><td>' + color + '</td> </tr>');
}

function formatDate(date) {
    return ((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes());
}

function updateBgColor(color){
    $('body').css('background', bgColors[color]);
}
